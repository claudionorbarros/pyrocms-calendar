<?php

/**
	* this file is part of a calendar module for pyrocms
	* Copyright (C) 2012  Aat Karelse <aat@vuurrosmedia.nl>
	* This program is free software: you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation, either version 3 of the License, or
    * (at your option) any later version.

    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.

    * You should have received a copy of the GNU General Public License
    * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * This is a calendar module for PyroCMS
 *
 * @author 		Aat Karelse
 * @website		http://vuurrosmedia.nl
 * @package 	PyroCMS
 * @subpackage 	Calendar Module
 */

//messages
$lang['calendar:success']		=	'It worked';
$lang['calendar:error']			=	'It didn\'t work';
$lang['calendar:no_items']		=	'No Items';

//page titles

$lang['calendar:create']		=	'Create Item';
$lang['calendar:create_cat']    =   'Create Category';


//labels
$lang['calendar:name']			=	'Name';
$lang['calendar:date']			=	'Date';
$lang['calendar:description']	=	'Description';
$lang['calendar:price']			=	'Price';
$lang['calendar:support']		=	'Support';
$lang['calendar:item_list']		=	'Item List';
$lang['calendar:view']			=	'View';
$lang['calendar:edit']			=	'Edit';
$lang['calendar:delete']		=	'Delete';
$lang['calendar:dontshowtime']	= 	'Dont show hour and minutes.';
$lang['calendar:published']		= 	'Published ?';
$lang['calendar:new_event']     =   '+++';
$lang['calendar:stoptime']      =   'End at';
$lang['calendar:starttime']     =   'Start at';
$lang['calendar:location']      =   'Location';
$lang['calendar:categorie']     =   'Category';
$lang['calendar:events']        =   'Events';
$lang['calendar:categories']    =   'Categories';
$lang['calendar:year']          =   'Year';
$lang['calendar:week']          =   'Week';


//buttons
$lang['calendar:custom_button']	=	'Custom Button';
$lang['calendar:cancel']        =   'Cancel';
$lang['calendar:items']			=	'Items';
$lang['calendar:age']			=	'Minimal age';

//currency
$lang['calendar:currency']	    =	'&pound;';

//categories ladida                                

$lang['calendar:color']         =   "Color";
$lang['calendar:is_published']  =   "Published";
$lang['calendar:non_published'] =   "Unpublished";

$lang['calendar:January']       =   "January";
$lang['calendar:February']      =   "February";
$lang['calendar:March']         =   "March";
$lang['calendar:April']         =   "April";
$lang['calendar:May']           =   "May";
$lang['calendar:June']          =   "June";
$lang['calendar:July']          =   "July";
$lang['calendar:August']        =   "August";
$lang['calendar:September']     =   "September";
$lang['calendar:October']       =   "October";
$lang['calendar:November']      =   "November";
$lang['calendar:December']      =   "December";

$lang['calendar:Sunday']        =   "Sunday";
$lang['calendar:Monday']        =   "Monday";
$lang['calendar:Tuesday']       =   "Tuesday";
$lang['calendar:Wednesday']     =   "Wednesday";
$lang['calendar:Thursday']      =   "Thursday";
$lang['calendar:Friday']        =   "Friday";
$lang['calendar:Saturday']      =   "Saturday";

?>
