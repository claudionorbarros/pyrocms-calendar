<table width="100%" align="center" cellpadding="0" cellspacing="0" class="cal_table">
    <thead>
        <tr>
            <th> {{ year }} </th>
        </tr>
        <tr>
            <th> {{ week }} </th>           
        </tr>
    </thead>
    <tbody>
        {{ days }}
            <tr class="cal_week_tr">
               
                <td class="cal_week_td {{ today }}">
                
                {{ dayname }}

                 {{ events }}
                    <span><a style="border-left:8px solid #{{catcolor}}" href="{{link}}"> {{name}}</a></span>
                {{ /events }}
                </td>
            </tr>
        {{ /days }}
    </tbody>
</table>
<div class="legenda">
<ul>
{{legends}}
<li style="border-left:8px solid #{{color}}">{{name}}</li>
{{/legends}}

</ul>
</div>