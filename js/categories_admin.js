jQuery(function($){
	$("#colorpickerHolder").spectrum({
		change: function(color) {
			$("#colorpickerHolder").attr('value', color.toHexString());
		}
	});
});