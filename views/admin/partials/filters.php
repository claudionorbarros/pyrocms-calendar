<fieldset id="filters">
	<legend><?php echo lang('global:filters'); ?></legend>
	
	<?php echo form_open(''); ?>
	<?php echo form_hidden('f_module', $module_details['slug']); ?>
		<ul>  
			<li>
				<?php echo lang('calendar:status_label', 'f_status'); ?>
				<?php echo form_dropdown('f_status', array(0 => lang('global:select-all'), 1=>lang('calendar:non_published'), 2=>lang('calendar:is_published'))); ?>
			</li>
		
			<li>
				<?php echo lang('calendar:category_label', 'f_category'); ?>
				<?php echo form_dropdown('f_category', array(0 => lang('global:select-all')) + $categories); ?>
			</li>

			<li>
				<?php echo lang('calendar:starttime', 'f_starttime'); ?>
				<?php echo form_input('f_starttime', '', 'maxlength="10" id="datepicker" class="text width-20"'); ?> &nbsp;
			</li>

			<li>
				<?php echo lang('calendar:stoptime', 'f_stoptime'); ?>
				<?php echo form_input('f_stoptime', '', 'maxlength="10" id="datepicker2" class="text width-20"'); ?> &nbsp;
			</li>

			<li>
				<?php echo anchor(current_url() . '#', lang('calendar:cancel'), 'class="btn gray"') ?>
	
			</li>
			
		</ul>
	<?php echo form_close(); ?>
</fieldset>