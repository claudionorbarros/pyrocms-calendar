<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class ptcalendar {

    /**
     * Current selected year
     * @var int
     */
    private $_selected_year;
    
    /**
     * Current selected month
     * @var int
     */
    private $_selected_month;
    
    /**
     * CodeIgniter object
     * @var object
     */
    private $CI;
    
    /**
     * Day of week format, set in config file
     * @var string
     */
    private $day_of_week_format;
    
    /**
     * Month format, set in config file
     * @var string
     */
    private $month_format;
    
    /**
     * Year format, set in config file
     * @var string
     */
    private $year_format;

    /**
     * Constructor
     *
     * @access	public
     * @param	array	initialization parameters
     */
    public function __construct($config = array()) {      
        $this->CI = &get_instance();
        if (!empty($config)) {
            $this->initialize($config);
        }
    }

    // --------------------------------------------------------------------

    /**
     * Initialize Preferences
     *
     * @access	public
     * @param	array	initialization parameters
     * @return	void
     */
    private function initialize($config = array()) {
        foreach($config as $k => $v) {
            $this->{$k} = $v;
        }
    }


    /**
     * make the body of the week calendar
     * @param  datetime $time the time of the week generated
     * @param  array  $events the events in the week
     * @return string the html that is parsed
     */
    public function generate_week($time, $events = array())
    {

        $this->_week = $time->format('W');
        $this->_selected_year = $time->format('Y');
        $this->_selected_month = $time->format('m');
        $now = new DateTime("NOW");
        $this->_day_now = $now->format('d');
        $time->modify('+1 week');
        $this->_next_week = $time->format('W');
        $time->modify('-2 week');
        $this->_pref_week = $time->format('W');
        $this->_day = 5;

        $header = self::generate_week_header();
        $body = self::generate_week_body($events);
        $data = array_merge($header, $body);
        return $this->CI->parser->parse_string($this->template_week, $data, TRUE);
    }

    /**
     * generate the body of the month calendar
     * @param  datetime $time   the time of the month
     * @param  array  $events the events in this month
     * @return string the html that is parsed
     */
    public function generate($time, $events = array()) {
        // Set month and year
        if (!is_null($time))
            new DateTime();
        $this->_selected_year = $time->format('Y');
        $this->_selected_month = $time->format('m');
        $time->modify('+1 month');
        $this->_next_month = $time->format('m');
        $time->modify('-2 month');
        $this->_pref_month = $time->format('m');
        $header = self::generate_header();
        $body = self::generate_body($events);
        $data = array_merge($header, $body);
        return $this->CI->parser->parse_string($this->template, $data, TRUE);
    }


    /**
     * generate the header of the week calendar tabel
     * @return array with header data
     */
    public function generate_week_header() {
        $header = array();
        
        $header['week'] = $this->week_label . $this->_week;
        $header['nextweek'] = $this->_next_week;
        $header['prefweek'] = $this->_pref_week;
        $header['year'] =  $this->year_label .$this->_selected_year;

        if($this->_week == 52 || $this->_week == 1) {
            if($this->_week == 52) {
                $header['next_link'] = site_url($this->next_prev_url.'/week/'.($this->_selected_year + 1).'/1');
                $header['pref_link'] = site_url($this->next_prev_url.'/week/'.$this->_selected_year.'/'.($this->_week - 1));
            } else {
                $header['next_link'] = site_url($this->next_prev_url.'/week/'.$this->_selected_year.'/'.($this->_week + 1));
                $header['pref_link'] = site_url($this->next_prev_url.'/week/'.($this->_selected_year - 1).'/52');
            }
        } else {
            $header['next_link'] = site_url($this->next_prev_url.'/week/'.$this->_selected_year.'/'.($this->_week + 1));
            $header['pref_link'] = site_url($this->next_prev_url.'/week/'.$this->_selected_year.'/'.($this->_week - 1));
        }


        return $header;

    }

    /**
     * generate the body of the week calendar tabel
     * @param  array  $events the events that will be put in the week body calendar tabel
     * @return array with week body data
     */
    public function generate_week_body($events = array())
    {        

        $body = array();
        $body['legends'] = $events['legends'];

        $daysOfWeek = array(array('name'=>lang('calendar:Sunday')), array('name'=>lang('calendar:Monday')), array('name'=>lang('calendar:Tuesday')), array('name'=>lang('calendar:Wednesday')), array('name'=>lang('calendar:Thursday')), array('name'=>lang('calendar:Friday')), array('name'=>lang('calendar:Saturday')));

        for ($i=1;$i<=7;$i++)
        {

            $body['days'][$i]['daynr'] = 1;
            $body['days'][$i]['dayname'] = $daysOfWeek[$i-1]['name'];
            $body['days'][$i]['year'] = $this->_selected_year;
            $body['days'][$i]['week'] = $this->_week;
            $body['days'][$i]['month'] = $this->_selected_month;
            $body['days'][$i]['day'] = $i;
            $body['days'][$i]['predaylink'] = $this->preDaylink;

            if($i == date('j', time())-1 && $this->_week == date('W', time()) && $this->_selected_year == date('Y', time())) {
                $body['days'][$i]['today'] = "today";
            }            
            if (isset($events))
            foreach ($events as $event) 
            {

                if (isset($event['daynr']) && $event['daynr']-1 == $i )
                {
                    $body['days'][$i]['events'][] = $event;
                }
            }
        }
        return $body;
    }

    /**
     * Generates the month header data
     * @return array with month calendar header data
     */
    private function generate_header() {
        $header = array();

        // Set the selected month and year
        $header['month'] = lang('calendar:'.strftime($this->month_format, mktime(0,0,0,$this->_selected_month,1,0)));
        $header['nextmont'] = lang('calendar:'.strftime($this->month_format, mktime(0,0,0,$this->_next_month,1,0)));
        $header['prefmonth'] = lang('calendar:'.strftime($this->month_format, mktime(0,0,0,$this->_pref_month,1,0)));
        $header['year'] = strftime($this->year_format, mktime(0,0,0,1,1,$this->_selected_year));
        if($this->_selected_month == 12 || $this->_selected_month == 1) {
            if($this->_selected_month == 12) {
                $header['next_link'] = site_url($this->next_prev_url.'/'.($this->_selected_year + 1).'/1');
                $header['previous_link'] = site_url($this->next_prev_url.'/'.$this->_selected_year.'/'.($this->_selected_month - 1));
            } else {
                $header['next_link'] = site_url($this->next_prev_url.'/'.$this->_selected_year.'/'.($this->_selected_month + 1));
                $header['previous_link'] = site_url($this->next_prev_url.'/'.($this->_selected_year - 1).'/12');
            }
        } else {
            $header['next_link'] = site_url($this->next_prev_url.'/'.$this->_selected_year.'/'.($this->_selected_month + 1));
            $header['previous_link'] = site_url($this->next_prev_url.'/'.$this->_selected_year.'/'.($this->_selected_month - 1));
        }

        // Set the days of the week. Using locale.
        $header['daysofweek'] = array();
        $daysOfWeek = array(array('name'=>lang('calendar:Sunday')), array('name'=>lang('calendar:Monday')), array('name'=>lang('calendar:Tuesday')), array('name'=>lang('calendar:Wednesday')), array('name'=>lang('calendar:Thursday')), array('name'=>lang('calendar:Friday')), array('name'=>lang('calendar:Saturday')));
        foreach ($daysOfWeek as $day) {
            $header['daysofweek'][] = $day;
        }

        return $header;
    }

    /**
     * Generates the body data
     * @return array with body data
     */
    private function generate_body($events = array()) {
        $body = array();
    
        // Determine the total days in the month
        $totalDays = cal_days_in_month(CAL_GREGORIAN, $this->_selected_month, $this->_selected_year);
        // Determine which is the first weekday
        $date = new DateTime($this->_selected_year . "-" . $this->_selected_month . "-01");
        $date->modify('first day of');
        $startWeekDay = $date->format('w');

        $body['legends'] = $events['legends'];
        // Fill our calendar
        $body['weeks'] = array();
        $currentDayNumber = 1 - $startWeekDay; // Find the start
        $weekCounter = 0; // Needed to count the weeks to start new rows
        $dayCounter = 0; // Needed to count the days to start new rows
        while ($currentDayNumber <= $totalDays) {

            // Check if we reached the end of the week. In that case we need a new week
            if ($dayCounter == 7) {
                $dayCounter = 0;
                $weekCounter++;
            }

            // Check if we got a positive number. If not, we got a day from the previous month
            if ($currentDayNumber > 0) {
                // Let's see if we got some data to show. 
                if (isset($events[$currentDayNumber])) {
                    $body['weeks'][$weekCounter]['days'][$dayCounter]['month'] = $this->_selected_month;
                    $body['weeks'][$weekCounter]['days'][$dayCounter]['year'] = $this->_selected_year;
                    $body['weeks'][$weekCounter]['days'][$dayCounter]['day'] = $currentDayNumber;
                    $body['weeks'][$weekCounter]['days'][$dayCounter]['predaylink'] = $this->preDaylink;
                    foreach ($events[$currentDayNumber] as $event) {
                        $body['weeks'][$weekCounter]['days'][$dayCounter]['events'][] = $event;
                    }
                } else {
                    $body['weeks'][$weekCounter]['days'][$dayCounter]['month'] = $this->_selected_month;
                    $body['weeks'][$weekCounter]['days'][$dayCounter]['year'] = $this->_selected_year;
                    $body['weeks'][$weekCounter]['days'][$dayCounter]['day'] = $currentDayNumber;
                    $body['weeks'][$weekCounter]['days'][$dayCounter]['predaylink'] = $this->preDaylink;
                }
            } else {
                $body['weeks'][$weekCounter]['days'][$dayCounter]['day'] = '';
                $body['weeks'][$weekCounter]['days'][$dayCounter]['month'] = $this->_selected_month;
                $body['weeks'][$weekCounter]['days'][$dayCounter]['year'] = $this->_selected_year;
            }
            
            if($currentDayNumber == date('j', time()) && $this->_selected_month == date('n', time()) && $this->_selected_year == date('Y', time())) {
                $body['weeks'][$weekCounter]['days'][$dayCounter]['today'] = "today";
            }
            
            $dayCounter++;
            $currentDayNumber++;
        }

        // Fill the latest gaps with empty dates
        while ($dayCounter < 7) {
            $body['weeks'][$weekCounter]['days'][$dayCounter]['day'] = '';
            $body['weeks'][$weekCounter]['days'][$dayCounter]['month'] = $this->_selected_month;
            $body['weeks'][$weekCounter]['days'][$dayCounter]['year'] = $this->_selected_year;
            $dayCounter++;
        }
        return $body;
    }

}

// EOF