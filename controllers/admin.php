<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
	* this file is part of a calendar module for pyrocms
	* Copyright (C) 2012  Aat Karelse <aat@vuurrosmedia.nl>
	* This program is free software: you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation, either version 3 of the License, or
    * (at your option) any later version.

    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.

    * You should have received a copy of the GNU General Public License
    * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * This is a calendar module for PyroCMS
 *
 * @author 		Aat Karelse
 * @website		http://vuurrosmedia.nl
 * @package 	PyroCMS
 * @subpackage 	Calendar Module
 */
 
class Admin extends Admin_Controller
{
	protected $section = 'events';

	/**
	* Constructor method
	*
	* @access public
	* @return void
	*/
	 
	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('calendar_m');
		$this->load->model('calendar_categories_m');
		$this->lang->load('calendar');

		$this->form_validation->set_rules($this->calendar_m->event_validation_rules);
		
		$categories = array(0 => 'default');
		foreach ($this->calendar_categories_m->get_all() as $categorie)
		{
			$categories[$categorie->id] = $categorie->name;
		}

		$this->template
		->set('hours', array_combine($hours = range(0, 23), $hours))
		->set('minutes', array_combine($minutes = range(0, 59), $minutes))
		->set('categories',$categories)
		->append_metadata($this->load->view('fragments/wysiwyg'));
	}


	/**
	 * a callback funtion for form validation
	 *
	 * @access public
	 * @return boolean start date should be before stopdate
	 */
	public function compare_dates()
	{
		$startdate = DateTime::createFromFormat('Y-m-d H:i:s', $this->input->post('start_on')  . $this->input->post('start_on_hour') . ":" . $this->input->post('start_on_minute') . ":00");

		$stopdate = DateTime::createFromFormat('Y-m-d H:i:s', $this->input->post('stop_on')  . $this->input->post('stop_on_hour') . ":" . $this->input->post('stop_on_minute') . ":00");

		if ($startdate < $stopdate)
		{
			return true;
		}
		$this->form_validation->set_message('compare_dates', 'startdate is later than stopdate');
		return false;
	}
	 
	/**
	* List all existing calendars
	*
	* @access public
	* @return void
	*/
	public function index()
	{
		//set the base/default where clause
		$base_where = array();

		//add post values to base_where if f_module is posted
		if ($this->input->post('f_category')) 	$base_where['category'] = $this->input->post('f_category');
		if ($this->input->post('f_status')) 	$base_where['status'] 	= $this->input->post('f_status');
		if ($this->input->post('f_starttime')) 	$base_where['starttime'] = $this->input->post('f_starttime');
		if ($this->input->post('f_stoptime')) 	$base_where['stoptime'] = $this->input->post('f_stoptime');

		if($this->input->post('f_starttime') && $this->input->post('f_stoptime'))
		{
			$datetime1 = new DateTime($this->input->post('f_starttime'));
			$datetime2 = new DateTime($this->input->post('f_stoptime'));
			if ($datetime1 > $datetime2)
			{
				$this->session->set_flashdata('error', 'KOEI');
			}
		}

		$events = $this->calendar_m->get_many_by($base_where);
		$this->input->is_ajax_request() and $this->template->set_layout(FALSE);

		$this->template
		->title($this->module_details['name'])
		->append_js('admin/filter.js')
		->append_js('module::admin.js')
		->set_partial('filters', 'admin/partials/filters')
		->set_partial('events', 'admin/partials/events')
		// ->set('currenttime', $date->format('Y-m-d'))
		->set('events', $events);

		$this->input->is_ajax_request()
			? $this->template->build('admin/partials/events')
			: $this->template->build('admin/index');
	}
	
	/**
	* Create a new calendar
	*
	* @access public
	* @return void
	*/
	public function create()
	{
		if ($input = $this->input->post())
		{
			
			$starttime = DateTime::createFromFormat('Y-m-d H:i:s',$this->input->post('start_on') . " " . $this->input->post('start_on_hour') . ":" . $this->input->post('start_on_minute') . ":00");
			$stoptime = DateTime::createFromFormat('Y-m-d H:i:s',$this->input->post('stop_on') . " " . $this->input->post('stop_on_hour') . ":" . $this->input->post('stop_on_minute') . ":00");
			$data = $this->setrightpost($this->input->post());
			$data['starttime'] = $starttime->format('Y-m-d H:i:s');
			$data['stoptime'] = $stoptime->format('Y-m-d H:i:s');

			if ($id = $this->calendar_m->create($data) && $this->form_validation->run())
			{
				$this->session->set_flashdata('success', lang('calendar:success'));
				redirect('admin/calendar');
			}
			else
			{
				$this->session->set_flashdata('error', lang('calendar:error'));
				redirect('admin/calendar/create');
			}
		}
		else
		{
			foreach ($this->calendar_m->event_validation_rules AS $rule)
			{
				$data->{$rule['field']} = $this->input->post($rule['field']);
			}

			$date = new DateTime();
			$date->setTimestamp(now());
			$date->rightstarttime = $date->format('U');
			$date->rightstoptime = $date->format('U');
			$data->date = $date;
		}
		
		$this->pyrocache->delete('calendar_m');
		$this->template->title($this->module_details['name'], lang('calendar.new_event'))
		->set('rightstarttime', $date->format('U'))
		->set('rightstoptime', $date->format('U'))
		->build('admin/form', $data);	
	}
	
	/**
	* Edit an existing calendar
	*
	* @access public
	* @param int $id The ID of the calendar to edit
	* @return void
	*/
	public function edit($id = 0)
	{	
		$data = $this->calendar_m->get($id);
		
		if ($input = $this->input->post())
		{
			unset($_POST['btnAction']);
			$starttime = DateTime::createFromFormat('Y-m-d H:i:s',$this->input->post('start_on') . " " . $this->input->post('start_on_hour') . ":" . $this->input->post('start_on_minute') . ":00");
			$stoptime = DateTime::createFromFormat('Y-m-d H:i:s',$this->input->post('stop_on') . " " . $this->input->post('stop_on_hour') . ":" . $this->input->post('stop_on_minute') . ":00");
			$data = $this->setrightpost($this->input->post());
			$data['starttime'] = $starttime->format('Y-m-d H:i:s');
			$data['stoptime'] = $stoptime->format('Y-m-d H:i:s');
			if ($this->calendar_m->edit($id,$data) && $this->form_validation->run())
			{
				$this->session->set_flashdata('success', lang('calendar.success'));
				redirect('admin/calendar');
			}
			else
			{
				$this->session->set_flashdata('error', lang('calendar.error'));
			}
		}

		$date = new DateTime();
		$date->setTimestamp(now());
		if ($data->starttime && $data->stoptime)
		{
			$starttime = $date->createFromFormat('Y-m-d H:i:s', $data->starttime);
			$stoptime = $date->createFromFormat('Y-m-d H:i:s', $data->stoptime);
		}

		$this->pyrocache->delete('calendar_m');

		$this->template->title($this->module_details['name'], lang('calendar.edit'))
		->set('rightstarttime', $starttime->format('U'))
		->set('rightstoptime', $stoptime->format('U'))
		->build('admin/form', $data);
	}
	
	/**
	 * remove date stuff after it is integrated into something else
	 * @param  array $post post data in
	 * @return array $post cleaned data out
	 */
	private function setrightpost($post)
	{
		unset($post['start_on']);
		unset($post['start_on_hour']);
		unset($post['start_on_minute']);
		unset($post['stop_on']);
		unset($post['stop_on_hour']);
		unset($post['stop_on_minute']);
		unset($post['btnAction']);
		return $post;
	}

	/**
	 * pick the right action
	 * @return void
	 */
	public function action()
	{
		switch ($this->input->post('btnAction'))
		{
			case 'publish':
				$this->publish();
			break;
			
			case 'delete':
				$this->delete();
			break;
			
			default:
				redirect('admin/calendar');
			break;
		}
	}

	/**
	 * set a calendar / event life or the oposite
	 * @param  integer $id the id of the event to put life
	 * @return void
	 */
	public function publish($id = 0)
	{
		// Publish one
		$ids = ($id) ? array($id) : $this->input->post('action_to');

		if ( ! empty($ids))
		{
			// Go through the array of slugs to publish
			$post_titles = array();
			foreach ($ids as $id)
			{
				// Get the current page so we can grab the id too
				if ($post = $this->calendar_m->get($id))
				{
					$this->calendar_m->publish($id);
				}
			}
		}
		$this->pyrocache->delete('calendar_m');

		redirect('admin/calendar');
	}

	/**
	 * delete an event with id
	 * @param  integer $id guess what this id represents
	 * @return void
	 */
	public function delete($id = 0)
	{
		// Delete one
		$ids = ($id) ? array($id) : $this->input->post('action_to');
		// Go through the array of slugs to delete
		if ( ! empty($ids))
		{
			foreach ($ids as $id)
			{
				// Get the current page so we can grab the id too
				if ($post = $this->calendar_m->get($id))
				{
					$this->calendar_m->delete($id);
				}
			}
		}
	$this->pyrocache->delete('calendar_m');
	redirect('admin/calendar');
	}
}
