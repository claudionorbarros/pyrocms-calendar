<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
	* this file is part of a calendar module for pyrocms
	* Copyright (C) 2012  Aat Karelse <aat@vuurrosmedia.nl>
	* This program is free software: you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation, either version 3 of the License, or
    * (at your option) any later version.

    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.

    * You should have received a copy of the GNU General Public License
    * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * This is a calendar module for PyroCMS
 *
 * @author 		Aat Karelse
 * @website		http://vuurrosmedia.nl
 * @package 	PyroCMS
 * @subpackage 	Calendar Module
 */
 
class Widget_Calendar extends Widgets
{
	public $title		= array(
		'en' => 'calendar',
		'nl' => 'Kalender'
	);
	public $description	= array(
		'en' => 'Show a list of events per week.',
		'nl' => 'Laat een lijst met gebeurtenissen per week zien.'
	);
	public $author		= 'Aat Karelse';
	public $website		= 'http://vuurrosmedia.nl';
	public $version		= '1.2';
	
  
	/**
	* Run widget
	*
	* @access public
	* @return array
	*/
	public function run($options)
	{
		$prefs = array (
			'preDaylink' 		=> BASE_URL . $this->module_details['name'] . '/day',
			'show_next_prev'  	=> TRUE,
			'next_prev_url'   	=> $this->module_details['name'],
			'template' 			=>  $this->load->file(FCPATH  . SHARED_ADDONPATH . 'modules/calendar/views/calendar_template_view.php', true),
			'template_week'		=> $this->load->file(FCPATH  . SHARED_ADDONPATH . 'modules/calendar/views/calendar_week_template_view.php', true),
			'day_of_week_format' => "%A",
			'month_format' 		=> "%B",
			'year_format' 		=> "%Y",
			'year_label'		=> lang('calendar:year') . ": ",
			'week_label' 		=> lang('calendar:week'). ": "
		);
		
		$this->load->library('calendar/ptcalendar',$prefs);
		$newarray = array();
		$this->load->model('calendar/calendar_categories_m');
		$calendardate = new DateTime("NOW");
		$data->events = $this->calendar_m->get_all($calendardate->format('Y'),$calendardate->format('m'));
		


		if (count($data->events) > 0)
		{
			$interval = DateInterval::createFromDateString('1 day');
			foreach($data->events as $value)
			{

				if ($value->color == '')
				{
					$value->color = "888";
				}
				$startdate = DateTime::createFromFormat('Y-m-d H:i:s',$value->starttime);
				$stopdate = DateTime::createFromFormat('Y-m-d H:i:s',$value->stoptime);
				$endDateInt = new DateInterval( "P1D" );
				$stopdate->add( $endDateInt );

				$days     = new DatePeriod($startdate, $interval, $stopdate);
				foreach ( $days as $day ) {
					$newarray[] = array('daynr'=>(int)$day->format('d'), 'name'=>character_limiter($value->name, 5), 'link'=> BASE_URL . "calendar" . '/' . $value->id, 'catcolor' =>$value->color);
				}
			}
		}

		$legends = $this->calendar_categories_m->get_all_names();
		$newarray['legends'] = $legends;
		$data->acalendar = $this->ptcalendar->generate_week($calendardate, $newarray);
		return array('acalendar' => $data->acalendar,'options' => $options);
	}	
}
