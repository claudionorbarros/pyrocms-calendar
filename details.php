<?php defined('BASEPATH') or exit('No direct script access allowed');

/**
	* this file is part of a calendar module for pyrocms
	* Copyright (C) 2012  Aat Karelse <aat@vuurrosmedia.nl>
	* This program is free software: you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation, either version 3 of the License, or
	* (at your option) any later version.

	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	* GNU General Public License for more details.

	* You should have received a copy of the GNU General Public License
	* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * This is a calendar module for PyroCMS
 *
 * @author 		Aat Karelse
 * @website		http://vuurrosmedia.nl
 * @package 	PyroCMS
 * @subpackage 	Calendar Module
 */

class Module_Calendar extends Module {

	public $version = '1.2.0';
	const MIN_PHP_VERSION = '5.3.0';
	const MIN_PYROCMS_VERSION = '2.1';
	const MAX_PYROCMS_VERSION = '2.1.9';

	public function info()
	{
		return array(
			'name' => array(
				'en' => 'calendar'
			),
			'description' => array(
				'en' => 'This is a calendar',
				'nl' => 'Dit is een kalender'
			),
			'frontend' => TRUE,
			'backend' => TRUE,
			'menu' => 'content',
			'sections' => array(
				'events' => array(
					'name' 	=> 'calendar:events',
					'uri' 	=> 'admin/calendar',
						'shortcuts' => array(
							'create' => array(
								'name' 	=> 'calendar:create',
								'uri' 	=> 'admin/calendar/create',
								'class' => 'add'
								)
						)
					),
				'categories' => array(
					'name' 	=> 'calendar:categories',
					'uri' 	=> 'admin/calendar/categories',
						'shortcuts' => array(
							'create' => array(
								'name' 	=> 'calendar:create_cat',
								'uri' 	=> 'admin/calendar/categories/create',
								'class' => 'add'
								)
						)
					)
				)
		);
	}

	public function install()
	{
		if (!$this->check_php_version())
		{
			$this->session->set_flashdata('error', 'Need a higher php version !');
			return FALSE;
		}
		
		if (!$this->check_pyrocms_version())
		{
			$this->session->set_flashdata('error', 'Not the right PyroCMS version !');
			return FALSE;
		}
		

		$calendar = array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'auto_increment' => TRUE,
				'primary' => true
			),
			'starttime' => array(
				'type' => 'DATETIME',
				'null' => false
			),
			'stoptime' => array(
				'type' => 'DATETIME',
				'null' => false
			),
			'published' => array(
				'type' => 'TINYINT',
				'constraint' => 1,
				'default' => '0'
			),
			'name' => array(
				'type' => 'VARCHAR',
				'constraint' => 100
			),
			'description' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => false
			),
			'location' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => false
			),
			'price' => array(
				'type' => 'INT',
				'constraint' => 3,
				'default' => '0'
			),
			'categorie_id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'default' => '0'
			)
		);

		$calendar_categories = array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'auto_increment' => TRUE,
				'primary' => true
			),
			'name' => array(
				'type' => 'VARCHAR',
				'constraint' => 100
			),
			'description' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => false
			),
			'color' => array(
				'type' => 'VARCHAR',
				'constraint' => 6,
				'default' => '000000'
			)
		);

		$this->db->trans_start();

		$this->dbforge->add_field($calendar);
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('calendar');

		$this->dbforge->add_field($calendar_categories);
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('calendar_categories');
	
		
		$this->db->trans_complete();

		return $this->db->trans_status();
	}

	public function uninstall()
	{
		
		if ($this->db->delete('settings', array('module' => 'calendar')) 
			&& $this->dbforge->drop_table('calendar') 
			&& $this->dbforge->drop_table('calendar_categories'))
		{
			return TRUE;
		}

		return FALSE;
	}


	public function upgrade($old_version)
	{
		if ($this->uninstall())
		{
			if ($this->install())
			{
				return TRUE;
			}
		}

		return false;
	}

	public function help()
	{
		return "No documentation has been added for this module.<br />Contact the module developer for assistance.";
	}
	
	/**
	 * Check the current version of PHP and thow an error if it's not good enough
	 *
	 * @access private
	 * @return boolean
	 * @author Victor Michnowicz
	 */
	private function check_php_version()
	{
		// If current version of PHP is not up snuff
		if ( version_compare(PHP_VERSION, self::MIN_PHP_VERSION) < 0 )
		{
			show_error('This addon requires PHP version ' . self::MIN_PHP_VERSION . ' or higher.');
			return FALSE;
		}
	  return TRUE;
	}

	private function check_pyrocms_version()
	{
		if (version_compare(CMS_VERSION, self:: MIN_PYROCMS_VERSION) < 0 && version_compare(CMS_VERSION, self:: MAX_PYROCMS_VERSION) > 0)
		{
			show_error('This addon requires PYROCMS version ' . self::MIN__VERSION . ' minnimal or version '. MAX_PYROCMS_VERSION .  'maximal.');
			return FALSE;
		}
		return TRUE;
	}
}
/* End of file details.php */
