jQuery(function($){
	$("#datepicker2").datepicker({dateFormat: 'yy-mm-dd'});
	$filter_form	: $('#filters form'),
	$('input[type="text"]', pyro.filter.$filter_form).on('change', function()
	{
		//build the form data
		form_data = pyro.filter.$filter_form.serialize();
		//fire the query
		pyro.filter.do_filter(pyro.filter.f_module, form_data);
	});
});
