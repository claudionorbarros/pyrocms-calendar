	<?php if (!empty($events)): ?>
	
		<table>
			<thead>
				<tr>
					<th><?php echo form_checkbox(array('name' => 'action_to_all', 'class' => 'check-all'));?></th>
					<th><?php echo lang('calendar:name'); ?></th>
					<th><?php echo lang('calendar:starttime'); ?></th>
					<th><?php echo lang('calendar:stoptime'); ?></th>
					<th></th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<td colspan="5">
						<div class="inner"><?php $this->load->view('admin/partials/pagination'); ?></div>
					</td>
				</tr>
			</tfoot>
			<tbody>
				<?php foreach( $events as $event ): ?>
				<tr>
					<td><?php echo form_checkbox('action_to[]', $event->id); ?></td>
					<td><?php echo $event->name; ?></td>
					<td><?php echo $event->starttime; ?></td>
					<td><?php echo $event->stoptime; ?></td>
					<td class="actions">
						<?php echo
						anchor('calendar/'.$event->id, lang('calendar:view'), 'class="btn green" target="_blank"').' '.
						anchor('admin/calendar/edit/'.$event->id, lang('calendar:edit'), 'class="btn orange"'); ?>
					</td>
				</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
		
		
	<?php else: ?>
		<div class="no_data"><?php echo lang('sample:no_events'); ?></div>
	<?php endif;?>


