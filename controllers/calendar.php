<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
	* this file is part of a calendar module for pyrocms
	* Copyright (C) 2012  Aat Karelse <aat@vuurrosmedia.nl>
	* This program is free software: you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation, either version 3 of the License, or
	* (at your option) any later version.

	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	* GNU General Public License for more details.

	* You should have received a copy of the GNU General Public License
	* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * This is a calendar module for PyroCMS
 *
 * @author 		Aat Karelse
 * @website		http://vuurrosmedia.nl
 * @package 	PyroCMS
 * @subpackage 	Calendar Module
 */
 
class Calendar extends Public_Controller
{
	/**
	* Constructor method
	*
	* @access public
	* @return void
	*/
	public function __construct()
	{
		parent::__construct();
		$this->load->model('calendar_m');

		$this->lang->load('calendar');
		
		$prefs = array (
			'preDaylink' 		=> BASE_URL . $this->module_details['name'] . '/day',
			'show_next_prev'  	=> TRUE,
			'next_prev_url'   	=> $this->module_details['name'],
			'template' 			=> $this->load->file(FCPATH  . SHARED_ADDONPATH . 'modules/calendar/views/calendar_template_view.php', true),
			'template_week'		=> $this->load->file(FCPATH  . SHARED_ADDONPATH . 'modules/calendar/views/calendar_week_template_view.php', true),
			'day_of_week_format' => "%A",
			'month_format' 		=> "%B",
			'year_format' 		=> "%Y",
			'year_label'		=> lang('calendar:year') . ": ",
			'week_label' 		=> lang('calendar:week'). ": "
		);
		$this->load->library('ptcalendar',$prefs);
		
		$this->template->append_css('module::calendar.css');
	}

	/**
	 * a controller for a wek calndar instead of a month calendar, cant do a next and pref week
	 * @access public
	 * @return void
	 */
	public function week()
	{
		$this->load->model('calendar_categories_m');
		$newarray = array();
		$calendardate = new DateTime("NOW");

		if (!$data->events = $this->pyrocache->model('calendar_m', 'get_all', array($calendardate->format('Y'), $calendardate->format('m')), 120))
		{
			$data->events = $this->calendar_m->get_all($calendardate->format('Y'),$calendardate->format('m'));
			$this->pyrocache->write($legends, 'calendar_m');
		}
		if(!$legends = $this->pyrocache->get('calendar_categories_m') )
		{
			$legends = $this->calendar_categories_m->get_all_names();
			$legends[] = array('name'=>'default','color'=>'888');
			$this->pyrocache->write($legends, 'calendar_categories_m');
		}
		if (count($data->events) > 0)
		{
			$interval = DateInterval::createFromDateString('1 day');
			foreach($data->events as $value)
			{
				if ($value->color == '')
				{
					$value->color = "888";
				}

				$startdate = DateTime::createFromFormat('Y-m-d H:i:s',$value->starttime);
				$stopdate = DateTime::createFromFormat('Y-m-d H:i:s',$value->stoptime);
				$days     = new DatePeriod($startdate, $interval, $stopdate);
				foreach ( $days as $day ) 
				{
					$newarray[] = array('daynr'=>(int)$day->format('d'), 'name'=>character_limiter($value->name, 5), 'link'=> BASE_URL . $this->module_details['name'] . '/' . $value->id, 'catcolor' =>$value->color);
				}
			}
		}

		$newarray['legends'] = $legends;
	
		$this->template->set_breadcrumb($this->module_details['name']);
		$data->acalendar = $this->ptcalendar->generate_week($calendardate, $newarray);

		$this->template->title($this->module_details['name'], 'the rest of the page title')
		->build('index', $data);
	}

	/**
	 * Index method
	 *
	 * @access public
	 * @return void
	 */
	public function index()
	{
		$this->load->model('calendar_categories_m');
		$newarray = array();
		
		if ($this->uri->segment(2) && $this->uri->segment(3))
		{
			$calendardate = DateTime::createFromFormat('Y-m-d', $this->uri->segment(2) . '-' . $this->uri->segment(3) . "-1");
		}
		else
		{
			$calendardate = new DateTime("NOW");
		}

		if (!$data->events = $this->pyrocache->model('calendar_m', 'get_all', array($calendardate->format('Y'), $calendardate->format('m')), 120))
		{
			$data->events = $this->calendar_m->get_all($calendardate->format('Y'),$calendardate->format('m'));
			$this->pyrocache->write($data->events, 'calendar_m');
		}
		if(!$legends = $this->pyrocache->get('calendar_categories_m', 'get_all_names') )
		{
			$legends = $this->calendar_categories_m->get_all_names();
			$legends[] = array('name'=>'default','color'=>'888');
			$this->pyrocache->write($legends, 'calendar_categories_m');
		}


		if (count($data->events) > 0)
		{
			$interval = DateInterval::createFromDateString('1 day');
			foreach($data->events as $value)
			{

				if ($value->color == '')
					$value->color = "888";

				$startdate = DateTime::createFromFormat('Y-m-d H:i:s',$value->starttime);
				$stopdate = DateTime::createFromFormat('Y-m-d H:i:s',$value->stoptime);
				$endDateInt = new DateInterval( "P1D" );
				$stopdate->add( $endDateInt );

				$days     = new DatePeriod($startdate, $interval, $stopdate);
				foreach ( $days as $day ) {
					
					if (!isset($newarray[(int)$day->format('d')]) || count($newarray[(int)$day->format('d')]) < 2 )
					{
						$newarray[(int)$day->format('d')][] = array('name'=>character_limiter($value->name, 5), 'link'=> BASE_URL . $this->module_details['name'] . '/' . $value->id, 'catcolor' =>$value->color);
					}
				}
			}
		}
		
		$this->template->set_breadcrumb($this->module_details['name']);

		$newarray['legends'] = $legends;
		$data->acalendar = $this->ptcalendar->generate($calendardate, $newarray);
		// $data->acalendar = $this->ptcalendar->generate_week($calendardate, $newarray);

		$this->template->title($this->module_details['name'], 'the rest of the page title')
		->build('index', $data);
	}
	
	/**
	* View a single event
	*
	* @access public
	* @param string $id The id of the event
	* @return void
	*/
	public function event($id = '')
	{
		$item = $this->calendar_m->get_by('id', $id);
		if (isset($item))
		{
			$startdate = DateTime::createFromFormat('Y-m-d H:i:s',$item->starttime);
			$stopdate = DateTime::createFromFormat('Y-m-d H:i:s',$item->stoptime);
			$item->startday = $startdate->format('j M');
			$item->stopday = $stopdate->format('j M');
			$item->diffdays = $startdate->diff($stopdate)->days;
			$item->starthourmin = $startdate->format('H:i');
			$item->stophourmin =  $stopdate->format('H:i');
		}
		
		$this->template->title($this->module_details['name'], 'the rest of the page title')
		->set('item', $item)
		->build('extrasingelevent');
	}
	/**
	 * view a singel day with events
	 * @access public
	 * @param  Integer $year  realy ?
	 * @param  Integer $month REALY ?
	 * @param  Integer $day   guess what ?
	 * @return void
	 */
	public function day($year, $month, $day)
	{

		$items = $this->calendar_m->get_all($year,$month,$day);

		$date = DateTime::createFromFormat('Y-m-d',$year . '-' . $month . '-' . $day);

		$this->template->title($this->module_details['name'], 'the rest of the page title')
		->set('prelink', BASE_URL . $this->module_details['name'])
		->set('date',$date->format('d F Y'))
		->set('items', $items)
		->build('aday');
	}
}
